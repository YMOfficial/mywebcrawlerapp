package org.monzo.app.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This interface is a wrapper around the Jsoup client/html parser.
 */
@FunctionalInterface
public interface WebClient {
    Set<String> retrieveLinks(String url) throws IOException;

    static WebClient getDefault() {
        return url -> {
            Document doc = Jsoup.connect(url).get();
            Elements elements = doc.select("a[href]");
            return elements.stream().map(element -> element.absUrl("href")).collect(Collectors.toSet());
        };
    }
}
