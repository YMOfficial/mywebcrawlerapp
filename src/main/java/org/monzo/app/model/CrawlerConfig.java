package org.monzo.app.model;

import org.monzo.app.util.WebCrawlerUtil;

/**
 * This class contains the user defined inputs that serves the crawler with requirements
 */
public class CrawlerConfig {

    public final String startUrl;
    public final int maxPages;

    public final int timeoutMs;
    private CrawlerConfig(String startUrl, int maxPages, int timeoutMs) {
        this.startUrl = startUrl;
        this.maxPages = maxPages;
        this.timeoutMs = timeoutMs;
    }
    public static Builder Builder(){
        return new Builder();
    }

    public static class Builder {
        private String startUrl;
        private int maxPages;

        private int timeoutMs;

        public Builder() {
        }

        public Builder setStartUrl(String startUrl){
            this.startUrl = startUrl;
            return this;
        }

        public Builder setMaxPages(int maxPages){
            this.maxPages = maxPages;
            return this;
        }

        public Builder setTimeoutMs(int timeoutMs){
            this.timeoutMs = timeoutMs;
            return this;
        }

        public CrawlerConfig build(){

            if(!WebCrawlerUtil.isValidUrl(startUrl)){
                throw new IllegalArgumentException("Invalid url");
            }
            if(maxPages < 1){
                throw new IllegalArgumentException("Invalid maxPages");
            }
            if(timeoutMs < 1){
                throw new IllegalArgumentException("Invalid timeout");
            }

            return new CrawlerConfig(startUrl, maxPages, timeoutMs);
        }
    }

}
