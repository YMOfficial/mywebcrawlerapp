package org.monzo.app.util;

import org.junit.Test;

import static org.junit.Assert.*;


public class WebCrawlerUtilTest {
    @Test
    public void testValidSameDomainURL() {
        String url = "http://test.com/";
        String url1 = "http://test.com/page1?param1=value1";
        String url2 = "http://blog.test.com/page2";
        assertTrue(WebCrawlerUtil.isValidSameDomainURL(url, url1));
        assertFalse(WebCrawlerUtil.isValidSameDomainURL(url, url2));
    }

    @Test
    public void testUrlValidity() {
        String validUrl = "http://test.com/";
        String invalidUrl1 = "test.com";

        assertTrue(WebCrawlerUtil.isValidUrl(validUrl));
        assertFalse(WebCrawlerUtil.isValidUrl(invalidUrl1));
    }

    @Test
    public void testThatInternalReferenceIsStrippedFromUrl() {
        String internalRefUrl = "http://test.com/testing#subsection";
        String expectedUrl = "http://test.com/testing";
        assertEquals(WebCrawlerUtil.getAbsoluteURLWithoutInternalReference(internalRefUrl), expectedUrl);
    }
}