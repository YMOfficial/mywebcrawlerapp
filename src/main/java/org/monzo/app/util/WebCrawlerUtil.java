package org.monzo.app.util;

public class WebCrawlerUtil {
    public static final String POISON = "";

    static String URL_REMOVABLE_PROTOCOL_PATTERN = "http[s]?://";
    static String URL_REMOVABLE_URL_PATH_DELIMITER = "/";
    static String URL_REMOVABLE_URL_INTERNAL_LINK_DELIMITER = "#";
    static String URL_REMOVABLE_URL_PARAM_DELIMITER = "\\?";
    static String REPLACEABLE_EMPTY_STRING = "";

    public static boolean isValidSameDomainURL(String url1, String url2) {
        String currentDomain = getAbsoluteURLWithoutInternalReference(url1)
                .replaceAll(URL_REMOVABLE_PROTOCOL_PATTERN,REPLACEABLE_EMPTY_STRING)
                .split(URL_REMOVABLE_URL_PATH_DELIMITER)[0];
        String nextDomain = getAbsoluteURLWithoutInternalReference(url2)
                .replaceAll(URL_REMOVABLE_PROTOCOL_PATTERN, REPLACEABLE_EMPTY_STRING)
                .split(URL_REMOVABLE_URL_PATH_DELIMITER)[0];
        return currentDomain.equalsIgnoreCase(nextDomain);
    }

    /**
     * Strips internal reference from url. i.e links to subsections of page return base link
     * E.g: test.com/testing#subheading returns test.com/testing
     */
    public static String getAbsoluteURLWithoutInternalReference(String urlString) {
        return getAbsoluteURLWithoutQueryParams(urlString.split(URL_REMOVABLE_URL_INTERNAL_LINK_DELIMITER)[0]);

    }

    private static String getAbsoluteURLWithoutQueryParams(String urlString) {
        return urlString.split(URL_REMOVABLE_URL_PARAM_DELIMITER)[0];

    }

    public static boolean isValidUrl(String url){
        return url != null && url.startsWith("http");
    }

}
