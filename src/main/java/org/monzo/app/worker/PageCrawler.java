package org.monzo.app.worker;

import org.monzo.app.model.CrawlerData;
import org.monzo.app.model.PageData;
import org.monzo.app.util.WebClient;
import org.monzo.app.util.WebCrawlerUtil;

import java.io.IOException;
import java.util.Set;

/**
 * This is the Producer which crawls a url and feeds the eligible result to the url queue and
 * all the found urls to the crawler data
 */
public class PageCrawler implements Runnable {
    private final CrawlerData crawlerData;
    private final String url;
    private final WebClient webClient;

    public PageCrawler(CrawlerData crawlerData, String url, WebClient webClient) {
        this.crawlerData = crawlerData;
        this.url = url;
        this.webClient = webClient;
    }

    @Override
    public void run() {
        if(crawlerData.shouldCrawl(url) && crawlerData.addUrlToVisitedSet(url)){
            try {
                boolean shouldAddPoison = false;
                Set<String> links = webClient.retrieveLinks(url);
                PageData pageData = new PageData(url);
                for (String nextUrl : links) {
                    pageData.addFoundUrl(nextUrl);
                    if (!crawlerData.maxPagesExceeded()) {
                        crawlerData.addUrlToQueue(nextUrl);
                    }else
                        shouldAddPoison = true;
                }
                if(shouldAddPoison)
                    crawlerData.addUrlToQueue(WebCrawlerUtil.POISON);
                crawlerData.addPageToResult(pageData);
            } catch (IOException | InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }

    }

}
