package org.monzo.app.model;

import org.monzo.app.util.WebCrawlerUtil;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class contains all details about the crawling process,
 * most importantly, the Queue from which the consumer picks urls to crawl
 * and the set of results successfully crawled.
 */
public class CrawlerData {
    private final BlockingQueue<String> urlsToVisitQueue = new LinkedBlockingQueue<>();
    private final Set<String> visitedUrls = ConcurrentHashMap.newKeySet();

    private final Set<PageData> result = ConcurrentHashMap.newKeySet();

    private final AtomicInteger crawledPagesCount = new AtomicInteger(0);

    private final CrawlerConfig crawlerConfig;

    public CrawlerData(CrawlerConfig crawlerConfig){
        this.crawlerConfig = crawlerConfig;
        urlsToVisitQueue.add(crawlerConfig.startUrl);
    }
    public String getNextUrlOnQueue() throws InterruptedException {
        return urlsToVisitQueue.poll(crawlerConfig.timeoutMs, TimeUnit.MILLISECONDS);
    }

    public void addUrlToQueue(String url) throws InterruptedException {
        if(!urlIsVisited(url) && (isCrawlableUrl(url) || url.equals(WebCrawlerUtil.POISON)))
            urlsToVisitQueue.put(url);
    }

    public boolean addUrlToVisitedSet(String url){
        boolean success = visitedUrls.add(sanitizedUrl(url));
        if(success)
            crawledPagesCount.incrementAndGet();
        return success;
    }

    public boolean urlIsVisited(String url){
        return visitedUrls.contains(sanitizedUrl(url));
    }

    public boolean shouldCrawl(String url){
        return !maxPagesExceeded() && isCrawlableUrl(url) && !urlIsVisited(url); //would contain all the url verifications.
    }

    private boolean isCrawlableUrl(String url){
        return WebCrawlerUtil.isValidUrl(url) && WebCrawlerUtil.isValidSameDomainURL(crawlerConfig.startUrl, url);
    }

    public int getCrawledPagesCount() {
        return crawledPagesCount.getAcquire();
    }

    public boolean maxPagesExceeded() {
        return crawledPagesCount.getAcquire() >= crawlerConfig.maxPages;
    }

    public List<String> getUrlsToVisit() {
        return List.copyOf(urlsToVisitQueue);
    }

    public Set<String> getVisitedUrls() {
        return Collections.unmodifiableSet(visitedUrls);
    }


    public Set<PageData> getResult(){
        return Collections.unmodifiableSet(result);
    }

    public void addPageToResult(PageData pageData){
        result.add(pageData);
    }
    private String sanitizedUrl(String url){
        url = WebCrawlerUtil.getAbsoluteURLWithoutInternalReference(url);
        return url.startsWith("http://") ? url.replaceFirst("http://", "https://") : url;
    }

}
