package org.monzo.app.worker;

import org.junit.Test;
import org.monzo.app.model.CrawlerConfig;
import org.monzo.app.model.CrawlerData;
import org.monzo.app.util.WebClient;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;


public class PageCrawlerTest {

    private final String rootUrl = "https://test.com";
    @Test
    public void testThatUrlAddedToVisitedList() {
        Set<String> childSet = new HashSet<>();
        childSet.add("https://test.com/1");
        childSet.add("https://test.com/2");
        childSet.add("https://test.com/3");
        WebClient webClient = url -> childSet;
        String currentURL = "http://test.com/0";
        CrawlerConfig crawlerConfig = CrawlerConfig.Builder().setStartUrl(rootUrl).setMaxPages(5).setTimeoutMs(2).build();
        CrawlerData data = new CrawlerData(crawlerConfig);
        PageCrawler pageCrawler = new PageCrawler(data, currentURL, webClient);
        pageCrawler.run();
        assertFalse(data.getVisitedUrls().contains(currentURL));
        assertEquals(data.getUrlsToVisit().size(), 4);
        assertTrue(data.getUrlsToVisit().containsAll(childSet));
        assertTrue(data.getUrlsToVisit().contains(rootUrl));
        // because already crawled
        assertFalse(data.getUrlsToVisit().contains(currentURL));
    }

    @Test
    public void testThatInvalidUrlIsNotAddedToUrlQueue() {
        String invalidUrl = "test.com/1";
        String validUrl = "https://test.com/2";
        Set<String> childSet = new HashSet<>();
        childSet.add(invalidUrl);
        childSet.add(validUrl);

        WebClient webClient = url -> childSet;
        String currentURL = "http://test.com/0";
        CrawlerConfig crawlerConfig = CrawlerConfig.Builder().setStartUrl(rootUrl).setMaxPages(5).setTimeoutMs(2).build();
        CrawlerData data = new CrawlerData(crawlerConfig);
        PageCrawler pageCrawler = new PageCrawler(data, currentURL, webClient);
        pageCrawler.run();

        List<String> urlQueue = data.getUrlsToVisit();
        assertEquals(urlQueue.size(), 2);
        assertFalse(urlQueue.contains(invalidUrl));
        assertTrue(urlQueue.contains(validUrl));
    }
}