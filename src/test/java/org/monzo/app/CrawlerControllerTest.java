package org.monzo.app;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.monzo.app.controller.CrawlerController;
import org.monzo.app.model.CrawlerConfig;
import org.monzo.app.model.CrawlerData;
import org.monzo.app.model.PageData;
import org.monzo.app.util.WebClient;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;

public class CrawlerControllerTest {
    Future futureMock;
    ExecutorService executorServiceMock;
    CrawlerConfig cfg;
    CrawlerData crawlerData;
    @Before
    public void setUp() {
        // Create a mock future returned by executor. Since it determines when the process should end,
        // we run the task on the main thread and mock ExecutorService
        futureMock = Mockito.mock(Future.class);
        Mockito.when(futureMock.isDone()).thenReturn(true);

        executorServiceMock = Mockito.mock(ExecutorService.class);
        Mockito.when(executorServiceMock.submit(any(Runnable.class))).then(task -> {
            task.getArgumentAt(0, Runnable.class).run();
            return futureMock;
        });
        cfg = CrawlerConfig.Builder()
                .setStartUrl("https://test.com")
                .setMaxPages(5)
                .setTimeoutMs(1)
                .build();
        crawlerData = new CrawlerData(cfg);
    }

    @Test
    public void testUrlWithNoChildren() throws InterruptedException {

        // Web client that returns a page has no children
        WebClient webClient = url -> new HashSet<>();

        CrawlerController crawlerController = new CrawlerController(executorServiceMock, crawlerData, webClient);

        crawlerController.crawl();

        Set<PageData> pageData = crawlerData.getResult();
        assertEquals(1, pageData.size());
        PageData result = pageData.stream().findFirst().get();
        assertEquals("https://test.com", result.getPageUrl());
        assertEquals(0, result.getDomainUrlsFound().size());
        assertEquals(0, result.getNonDomainUrlsFound().size());
    }

    @Test
    public void testUrlWithChildren() throws InterruptedException {
        // Web client that returns a page and 3 children links.
        // firstCall ensures that children are only returned on the first call to web client,
        // so we have no nested pages
        AtomicBoolean firstCall = new AtomicBoolean(true);
        Set<String> childSet = new HashSet<>();
        childSet.add("https://test.com/1");
        childSet.add("https://test.com/2");
        childSet.add("https://test.com/3");
        WebClient webClient = url -> {
            if(firstCall.get()) {
                firstCall.set(false);
                return childSet;
            } else return new HashSet<>();
        };

        CrawlerController crawlerController = new CrawlerController(executorServiceMock, crawlerData, webClient);

        crawlerController.crawl();
        Set<PageData> pageData = crawlerData.getResult();
        assertEquals(4, pageData.size());
        Set<String> urlsRes = pageData.stream().map(PageData::getPageUrl).collect(Collectors.toSet());
        assertTrue(urlsRes.containsAll(childSet));
        assertTrue(urlsRes.contains("https://test.com"));
    }

    @Test
    public void testHttpAndHttpsDoesNotDoubleCrawl() throws InterruptedException {
        AtomicBoolean firstCalled = new AtomicBoolean(true);
        Set<String> childSet = new HashSet<>();
        childSet.add("https://test.com/1");
        childSet.add("http://test.com/1");

        WebClient webClient = url -> {
            if(firstCalled.get()) {
                firstCalled.set(false);
                return childSet;
            } else return new HashSet<>();
        };

        CrawlerController crawlerController = new CrawlerController(executorServiceMock, crawlerData, webClient);

        crawlerController.crawl();
        Set<PageData> pageData = crawlerData.getResult();
        assertEquals(2, pageData.size());
        Set<String> urlsRes = pageData.stream().map(PageData::getPageUrl).collect(Collectors.toSet());
        assertTrue(urlsRes.contains("https://test.com/1") || urlsRes.contains("http://test.com/1"));
        assertFalse(urlsRes.contains("https://test.com/1") && urlsRes.contains("http://test.com/1"));

    }

    @Test
    public void testInvalidChildUrlsDoNotGetCrawled() throws InterruptedException {
        AtomicBoolean firstCalled = new AtomicBoolean(true);
        Set<String> childSet = new HashSet<>();
        childSet.add("https://test.com/1");
        childSet.add("http://anotherDomain.com/1");

        WebClient webClient = url -> {
            if(firstCalled.get()) {
                firstCalled.set(false);
                return childSet;
            } else return new HashSet<>();
        };

        CrawlerController crawlerController = new CrawlerController(executorServiceMock, crawlerData, webClient);

        crawlerController.crawl();
        Set<PageData> pageData = crawlerData.getResult();
        assertEquals(2, pageData.size());
        Set<String> urlsRes = pageData.stream().map(PageData::getPageUrl).collect(Collectors.toSet());
        assertFalse(urlsRes.contains("http://anotherDomain.com/1"));
    }

    @Test
    public void testCrawlerTerminatesWhenMaxPagesReached() throws InterruptedException {
        AtomicBoolean firstCalled = new AtomicBoolean(true);
        Set<String> childSet = new HashSet<>();
        childSet.add("https://test.com/1");
        childSet.add("http://test.com/2");
        childSet.add("http://test.com/3");
        childSet.add("http://test.com/4");
        childSet.add("http://test.com/5");
        childSet.add("http://test.com/6");
        childSet.add("http://test.com/7");

        WebClient webClient = url -> {
            if(firstCalled.get()) {
                firstCalled.set(false);
                return childSet;
            } else return new HashSet<>();
        };

        CrawlerController crawlerController = new CrawlerController(executorServiceMock, crawlerData, webClient);

        crawlerController.crawl();
        Set<PageData> pageData = crawlerData.getResult();
        assertEquals(cfg.maxPages, pageData.size()); //Ensures that only maxPages count is crawled
    }
}
