package org.monzo.app.controller;

import org.monzo.app.model.CrawlerConfig;
import org.monzo.app.model.CrawlerData;
import org.monzo.app.model.PageData;
import org.monzo.app.util.WebClient;
import org.monzo.app.util.WebCrawlerUtil;
import org.monzo.app.worker.PageCrawler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * This class is responsible for instantiating the CrawlerData needed to store details from the crawling
 * and also consumes/picks up urls to crawl from the Queue
 */
public class CrawlerController {
    private final ExecutorService executorService;
    private final CrawlerData data;
    private final List<Future<?>> futureList;

    private final WebClient webClient;

    public CrawlerController(ExecutorService executorService, CrawlerData crawlerConfig, WebClient webClient) {
        this.executorService = executorService;
        this.data = crawlerConfig;
        this.futureList = new ArrayList<>();
        this.webClient = webClient;

    }
    public CrawlerController(CrawlerConfig cfg) {
        this(Executors.newFixedThreadPool(100), new CrawlerData(cfg), WebClient.getDefault());
    }
    public void crawl() throws InterruptedException {
        long startTime = System.currentTimeMillis();
        System.out.println("Crawling in progress...");

        String nextUrl = data.getNextUrlOnQueue();
        while (!isAllTasksCompleted(nextUrl) && !WebCrawlerUtil.POISON.equals(nextUrl)) {
            if(data.shouldCrawl(nextUrl)){
                Future<?> future = executorService.submit(new PageCrawler(data, nextUrl, webClient));
                futureList.add(future);
            }
            nextUrl = data.getNextUrlOnQueue();
        }

        long endTime = System.currentTimeMillis() - startTime;
        executorService.shutdownNow();
        System.out.println("finished Threads in: "+ (System.currentTimeMillis() - startTime));

        String result = data.getResult().stream().map(PageData::toString).collect(Collectors.joining());
        System.out.println(result);

        System.out.println("finished in: "+ endTime);
    }

    private boolean isAllTasksCompleted(String nextUrl){
        return nextUrl == null && isNoTaskPending();
    }
    private boolean isNoTaskPending(){
        Iterator<Future<?>> iterator = futureList.iterator();
        while (iterator.hasNext()){
            if (iterator.next().isDone()){
                iterator.remove();
            } else {
                return false;
            }
        }
        return true;
    }
}
