package org.monzo.app;

import org.monzo.app.controller.CrawlerController;
import org.monzo.app.model.CrawlerConfig;

/**
 * This App starts by taking a cfg requirements and crawls based on cfg given.
 */
public class Main {
    public static void main(String[] args) {
        try {
            CrawlerConfig crawlerConfig = CrawlerConfig.Builder()
                    .setStartUrl("https://monzo.com")
                    .setMaxPages(1000)
                    .setTimeoutMs(5 * 1000) // 5sec
                    .build();
            new CrawlerController(crawlerConfig).crawl();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}