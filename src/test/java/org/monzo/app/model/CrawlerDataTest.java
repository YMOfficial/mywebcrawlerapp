package org.monzo.app.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class CrawlerDataTest {
    CrawlerConfig cfg;
    CrawlerData crawlerData;
    String rootUrl = "https://test.com";
    @Before
    public void setUp() {
        cfg = CrawlerConfig.Builder()
                .setStartUrl(rootUrl)
                .setMaxPages(3)
                .setTimeoutMs(1)
                .build();
        crawlerData = new CrawlerData(cfg);
    }

    @Test
    public void testThatUnmatchedUrlIsNotCrawled() {

        String urlMatchedDomain = rootUrl + "/correct";
        String urlUnmatchedDomain = "https://tester.com";

        assertTrue(crawlerData.shouldCrawl(urlMatchedDomain));
        assertFalse(crawlerData.shouldCrawl(urlUnmatchedDomain));
    }

    @Test
    public void testThatUrlAlreadyVisitedIsNotCrawled() {
        String visitedUrl = rootUrl + "/one";
        String unVisitedUrl = rootUrl + "/two";

        crawlerData.addUrlToVisitedSet(visitedUrl);

        assertFalse(crawlerData.shouldCrawl(visitedUrl));
        assertTrue(crawlerData.shouldCrawl(unVisitedUrl));
    }

    @Test
    public void testThatUrlIsNotCrawledWhenMaxPagesExceeded() {
        String visitedUrl1 = rootUrl + "/one";
        String visitedUrl2 = rootUrl + "/two";
        String visitedUrl3 = rootUrl + "/three";
        String visitedUrl4 = rootUrl + "/four";

        // Add 3 urls to max out visited pages
        crawlerData.addUrlToVisitedSet(visitedUrl1);
        crawlerData.addUrlToVisitedSet(visitedUrl2);
        crawlerData.addUrlToVisitedSet(visitedUrl3);

        // Test that excess url is not crawled
        assertEquals(cfg.maxPages, crawlerData.getCrawledPagesCount());
        assertEquals(crawlerData.getVisitedUrls().size(), crawlerData.getCrawledPagesCount());
        assertFalse(crawlerData.shouldCrawl(visitedUrl4));
    }

    @Test
    public void testThatInternalReferenceUrlIsNotCrawledIfParentUrlIsAlreadyVisited() {
        String internalRefUrl = "http://test.com/testing#subsection";
        String parentUrl = "http://test.com/testing";
        crawlerData.addUrlToVisitedSet(parentUrl);

        assertFalse(crawlerData.shouldCrawl(internalRefUrl));
    }

    @Test
    public void testThatParentUrlIsVisitedWhenInternalUrlIsCrawled() {
        String internalRefUrl = "http://test.com/testing#subsection";
        String parentUrl = "http://test.com/testing";
        crawlerData.addUrlToVisitedSet(internalRefUrl);

        assertTrue(crawlerData.urlIsVisited(parentUrl));
    }
}