package org.monzo.app.model;

import org.monzo.app.util.WebCrawlerUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * This class represents the result of a successfully crawled url.
 */
public class PageData {
    private final String pageUrl;
    private final Set<String> domainUrlsFound = new HashSet<>();
    private final Set<String> nonDomainUrlsFound = new HashSet<>();

    public PageData(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public Set<String> getDomainUrlsFound() {
        return domainUrlsFound;
    }

    public Set<String> getNonDomainUrlsFound() {
        return nonDomainUrlsFound;
    }

    public void addFoundUrl(String url) {
        if(WebCrawlerUtil.isValidUrl(url)) {
            if(WebCrawlerUtil.isValidSameDomainURL(pageUrl, url)) {
                domainUrlsFound.add(url);
            } else {
                nonDomainUrlsFound.add(url);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Page url: " + pageUrl + ":\n  Domain Urls Found:\n");
        domainUrlsFound.forEach(url -> result.append("    ").append(url).append("\n"));
        result.append("  Non Domain Urls Found:\n");
        nonDomainUrlsFound.forEach(url -> result.append("    ").append(url).append("\n"));
        return result.toString();
    }

}
